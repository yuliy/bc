bc
----
### About
An app which generates embedded players for all the albums of artists a user is following on bandcamp.

### Install
* Run `npm install` in the top level directory to setup all Node.js dependencies
* Run `bower install` in the top level directory to setup all JS/CSS library dependencies

### Usage
* Create a file in the top level directory called `config.js` based on the configuration template `config.js.template`.  **You will need to supply your own Bandcamp API Key to be able to run a server**
* To run the server run `node app.js` in the top level directory.  You can now reach the application by going to `localhost:3000/`