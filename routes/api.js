var jsdom = require('jsdom');
var request = require('request');
var config = require('../config.js');

/**
 * Generate HTML for an embedded player for an Album given an Album ID
 *
 * @param {int} id Album ID to generate the Embedded Player for
 * @param {String} HTML for the Embedded Player
 */
function _getAlbumEmbedById(id)
{
    var link = '"http://bandcamp.com/EmbeddedPlayer/album=' + id + '/size=large/bgcol=ffffff/linkcol=0687f5/transparent=true/"';
    var embed = '<iframe style="border: 0; width: 203px; height: 460px; display: inline-block;" src=' + link + ' seamless></iframe>';
    return embed;
}

/**
 * Generate HTML for an embedded player for a Track given a Track ID
 *
 * @param {int} id Track ID to generate the Embedded Player for
 * @param {String} HTML for the Embedded Player
 */
function _getSingleEmbedById(id)
{
    var link = '"http://bandcamp.com/EmbeddedPlayer/track=' + id + '/size=large/bgcol=ffffff/linkcol=0687f5/notracklist=true/transparent=true/"';
    var embed = '<iframe style="border: 0; width: 203px; height: 460px;" src=' + link + ' seamless></iframe>';
    return embed;
}

/**
 * Generate the URL for the API endpoint to retrieve discogaphy data based on a Band ID
 *
 * @param {int} id Band ID to retrieve the discography data for
 * @return {String} URL for the API endpoint to retrieve the discography data
 */
function _getDiscographyApiUrl(id)
{
    return "http://api.bandcamp.com/api/band/3/discography?key=" + config.bandcamp.key + "&band_id=" + id + "&debug";
}

/**
 * Retrieve the Following Collection for a User given a Username in the Request Object.  Send
 * the collection back through the response as JSON to be consumed on the frontend.
 *
 * @param {Object} req Request Object
 * @param {Object} res Response Object
 */
function following(req, res)
{
    // Tell the request that we want to fetch a bandcamp following page, send the results to a callback function
    request({
        uri: 'https://bandcamp.com/' + req.query.name + '/following'
    },
    function(err, response, body)
    {
        var self = this;
        self.items = new Array();
        
        // Check to ensure there was no obvious error
        if(err && response.statusCode !== 200)
        {
            console.log('Request error.');
            return;
        }

        // Send the body param as the HTML code we will parse in jsdom
        // also tell jsdom to attach jQuery in the scripts and loaded from jQuery.com
        jsdom.env({
            html: body,
            scripts: [config.jquery.url],
            done: function(err, window)
            {
                // Use jQuery just as in a regular HTML page
                var $ = window.jQuery;
                var $artists = $("body #following-artists-container .follow-grid .follow-grid-item");

                // Iterate over all the bands in the Following Grid and prepare the Following information for rendering
                $artists.each(
                    function(index)
                    {
                        // format of follow-grid-item id is "follow-grid-item-band_<band_id>"
                        var bandId = $(this).attr("id").split("_")[1];
                        self.items[index] = {
                            id: index,
                            name: $(this).find(".band-name").text(),
                            band_id: bandId,
                            href: "/discography?id=" + bandId,
                            img: $(this).children("a").find("img").attr("data-original"),
                        };
                    }
                );

                res.send(self.items);
            }
        });
    });
}

/**
 * Retrieve the Discography Collection for a Band Given a Band ID in the Request Object.  Send
 * the collection back through the response as JSON to be consumed on the frontend.
 *
 * @param {Object} req Request Object
 * @param {Object} res Response Object
 */
function discography(req, res)
{
    request({
        uri: _getDiscographyApiUrl(req.query.id)
    }, function(err, response, body)
    {
        // Check to ensure there was no obvious error
        if(err && response.statusCode !== 200)
        {
            console.log("Request error: failure to retrieve page");
            return;
        }

        var data = JSON.parse(body);

        // Check if the Response data has an error code
        if (data["error"])
        {
            console.log("Request error: " + data["error_message"]);
            return;
        }

        // Retrieve Discography data from the response
        var discography = data["discography"];

        // Sort the Albums by the release_date parameter
        discography.sort(function(a, b) {
            return b["release_date"] - a["release_date"];
        });

        // Prepare the Discography for Rendering
        for (var index in discography)
        {
            var entry = discography[index];
            if (entry["album_id"]) // check if the result is an album
            {
                entry["embed"] = _getAlbumEmbedById(entry["album_id"]);
            } else 
            {
                // if the result isn't linked to an album it might be a single track
                if (entry["track_id"])
                {
                    entry["embed"] = _getSingleEmbedById(entry["track_id"]);
                }
            }

            entry['id'] = index;
        }

        res.send(discography);
    });
}

exports.following = following;
exports.discography = discography;
