/**
 * Module dependencies.
 */

var express    = require('express');
var bodyParser = require('body-parser');
var api        = require('./routes/api');
var utils      = require('./utils');
var config     = require('./config');
var http       = require('http');
var path       = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(bodyParser.json());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'frontend')));

// development only
if ('development' == app.get('env'))
{
    app.use(express.errorHandler());
}

app.get("/", utils.basicAuth(config.httpAuth.username, config.httpAuth.password));
app.get("/api/following", api.following);
app.get("/api/discography", api.discography);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
