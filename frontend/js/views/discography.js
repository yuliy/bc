define([
    'jquery', 
    'underscore', 
    'backbone',
    'collections/discography',
    'views/album'
    ], function($, _, Backbone, Discography, AlbumView) {
    var DiscographyView = Backbone.View.extend({
        el: '#music div#discography',

        /**
         * Initialize this view by loading a new Discography collection using the Band ID provided.
         *
         * @param {string} bandId Bandcamp Band ID for which we are retrieving the discography collection.
         */
        initialize: function(bandId) {
            this.collection = new Discography();

            // pass through the bandId as a parameter to be used in the API call
            this.collection.fetch(
                {
                    reset: true,
                    data: $.param(
                        {
                            id: bandId
                        }
                    )
                }
            );
            this.render();

            this.listenTo(this.collection, 'reset', this.render);
        },

        /**
         * Rendering function for displaying the discographt collection.  We will iterate over all the albums
         * in the collection and render each one individually.
         */
        render: function() {
            this.$el.html("");
            this.collection.each(function(item) {
                this.renderAlbum(item);
            }, this );
        },

        /**
         * Render each Album individually by generating a new Album View and inserting it into the 
         * Discography View.
         *
         * @param {Backbone.Model} item The Album Model to be rendered
         */
        renderAlbum: function(item) {
            var albumView = new AlbumView({
                model: item
            });
            this.$el.append( albumView.render().el );
        }
    });
    return DiscographyView;
});

