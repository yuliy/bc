define([
    'jquery', 
    'underscore', 
    'backbone',
    'text!templates/album.html'
    ], function($, _, Backbone, albumTemplate){
    var AlbumView = Backbone.View.extend({
        tagName: 'div',
        className: 'album',
        template: _.template(albumTemplate),

        /**
         * Rendering function for rendering an Album Model, done by simply passing the model attributes to
         * the Album Template.
         */
        render: function() {
            this.$el.html( this.template( this.model.attributes ) );
            return this;
        }
    });
    return AlbumView;
});