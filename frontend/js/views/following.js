define([
    'jquery', 
    'underscore', 
    'backbone',
    'collections/following',
    'views/band',
    'text!templates/following.html'
    ], function($, _, Backbone, Following, BandView, followingTemplate) {
    var FollowingView = Backbone.View.extend({
        template: _.template(followingTemplate),

        // Setup events for filtering the Following collection
        events: {
            'submit #filter-form': 'filter',
            'keyup #filter-input': 'filter',
            'keydown #filter-input': 'filter',
            'click #clear_button': 'filter'
        },

        /**
         * Initialize this view by loading a new Following Collection using a provided username.
         *
         * @param {string} name Bandcamp username for which we are retrieving the following collection.
         */
        initialize: function(name) {
            this.username = name;

            this.collection = new Following();

            // pass through the username to be used as a parameter in the API call
            this.collection.fetch(
                {
                    reset: true,
                    data: $.param(
                        {
                            name: name
                        }
                    )
                }
            );

            this.listenTo(this.collection, 'reset', this.render);
        },

        /**
         * Rendering function for displaying the following collection.  We will iterate over all the bands
         * in the collection and render each one individually.
         */
        render: function() {
            // render the template for this element passing through the bandcamp username to generate the banner
            this.$el.html(
                this.template(
                    {
                        username: this.username,
                        url: 'https://bandcamp.com/' + this.username + '/following'
                    }
                )
            );

            $(".loader").show();

            // iterate over the band collection and render each individually
            this.collection.each(function(item) {
                this.renderBand(item);
            }, this );

            $(".loader").hide();

            return this;
        },

        /**
         * Render each Band individually by generating a new Band View and inserting it into the band list
         * element in the Following View.
         *
         * @param {Backbone.Model} item The Band Model to be rendered
         */
        renderBand: function(item) {
            var bandView = new BandView({
                model: item
            });
            this.$el.find('#band_list ul').append( bandView.render().el );

            this.$el.find('#band-list-filter').show();
        },

        /**
         * Function to filter the band collection.  This is done by retrieving the filter string off of the
         * filter input and checking it against the name of every band in the list, hiding those that do not
         * match the beginning of the filter string.
         *
         * @param {Object} The event which triggered this function call.
         */
        filter: function(e) {
            if (e.type == "submit") {
                e.preventDefault();
            }

            if ((e.type == "keydown" && e.keyCode == 27) ||
                (e.type == "click" && e.currentTarget.id == "clear_button")
            ) {
                this.$el.find('#filter-input').val("");
            }

            // retrieve the string to filter by from the filter input
            var filterBy = this.$el.find('#filter-input').val().toLowerCase(); 

            // iterate over each band and check the name against the filter string
            $("span.band").each(
                function(index, element) {
                    var bandElement = $(element);

                    // retrieve the band name from the band element
                    var bandName = bandElement.find("span span").html();
                    bandName = bandName.toLowerCase();

                    // check if the filter by is within the band name
                    if (bandName.indexOf(filterBy) == -1) {
                        bandElement.hide();
                    } else {
                        bandElement.show();
                    }
                }
            );
        }
    });
    return FollowingView;
});

