define([
    'jquery', 
    'underscore', 
    'backbone',
    'text!templates/header.html'
    ], function($, _, Backbone, headerTemplate) {
    var HeaderView = Backbone.View.extend({
        template: _.template(headerTemplate),

        /**
         * Rendering function for the Header View.
         */
        render: function() {
            this.$el.html(this.template({}));
            return this;
        }
    });
    return HeaderView;
});