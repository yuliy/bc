define([
    'jquery', 
    'underscore', 
    'backbone',
    'text!templates/about.html'
    ], function($, _, Backbone, aboutTemplate) {
    var AboutView = Backbone.View.extend({
        template: _.template(aboutTemplate),

        /**
         * Rendering function for the About View.
         */
        render: function() {
            this.$el.html(this.template({}));
            return this;
        }
    });
    return AboutView;
});

