define([
    'jquery', 
    'underscore', 
    'backbone',
    'text!templates/default.html'
    ], function($, _, Backbone, defaultTemplate) {
    var DefaultView = Backbone.View.extend({
        template: _.template(defaultTemplate),

        /**
         * Rendering function for the Default View.
         */
        render: function() {
            this.$el.html(this.template({}));
            return this;
        }
    });
    return DefaultView;
});

