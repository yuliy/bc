define([
    'jquery', 
    'underscore', 
    'backbone',
    'views/discography',
    'text!templates/band.html'
    ], function($, _, Backbone, DiscographyView, bandTemplate){
    var BandView = Backbone.View.extend({
        tagName: 'li',
        template: _.template(bandTemplate),

        // Setup the event to render the discography for this Band
        events: {
            'click .band': 'renderDiscography'
        },

        /**
         * Rendering function for rendering a Band Model, done by simply passing the model attributes to
         * the Band Template.
         */
        render: function() {
            this.$el.html(this.template(this.model.attributes));
            return this;
        },

        /**
         * Function for rendering the discography for this band.  This is done by loading a new Discography
         * View based off of the Band ID associate to this View.
         */
        renderDiscography: function() {
            // do not re-render the discography if we are selecting the already selected band
            if ($(".band.selected").attr("data-band-id") == this.model.attributes["band_id"]) {
                return;
            }

            // fade the existing discography view out and then display the new discography view
            var that = this;
            $("#discography").hide
            (
                200,
                function() {
                    new DiscographyView(that.model.attributes['band_id']);
                    $("#discography").show();
                }
            );
            $(".band.selected").removeClass("selected"); // remove the selected class off the last selected item
            this.$el.find(".band").addClass("selected"); // add the selected class to our newly selected item   
        }
    });
    return BandView;
});