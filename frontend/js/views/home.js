define([
    'jquery', 
    'underscore', 
    'backbone',
    'text!templates/home.html'
    ], function($, _, Backbone, homeTemplate) {
    var HomeView = Backbone.View.extend({
        template: _.template(homeTemplate),

        // Setup events for clearing and submitting the username entry form.
        events: {
            'click #username': 'clearForm',
            'submit #usernameform': 'submitForm'
        },

        /**
         * Rendering function for the Home View.
         */
        render: function() {
            this.$el.html(this.template([]));
            return this;
        },

        /**
         * Function to clear the username entry form.
         */
        clearForm: function() {
            $("#username").attr("value","");
        },

        /**
         * Function to submit the username entry form, this is move the location of the browser to the
         * following route using the input of the username input field.
         */
        submitForm: function() {
            event.preventDefault();
            window.location = "#following/" + $("#username").val();
        }
    });
    return HomeView;
});

