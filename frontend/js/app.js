require.config({
  	baseUrl:'js/',
  	paths: {
    	jquery: 'lib/jquery/dist/jquery.min',
    	underscore: 'lib/underscore/underscore-min',
    	backbone: 'lib/backbone/backbone',
    	text: 'lib/requirejs-text/text'
  	}
});

require(['jquery', 'router'], function($, AppRouter){
    // Instantiate the router
    var app_router = new AppRouter;

    // Start Backbone history a necessary step for bookmarkable URL's
    Backbone.history.start();
});