define([
  	'underscore', 
  	'backbone', 
  	'models/album'
  	], function(_, Backbone, Album){
      
	var Discography = Backbone.Collection.extend({
		model: Album,
		url: function() {
			return '/api/discography'
		}
	});
    return Discography;
});