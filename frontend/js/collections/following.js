define([
  	'underscore', 
  	'backbone', 
  	'models/band'
  	], function(_, Backbone, Band){
    
	var Following = Backbone.Collection.extend({
		model: Band,
		url: function() {
			return '/api/following'
		}
	});
    return Following;
});