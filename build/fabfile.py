from __future__ import with_statement

import glob
import json
import os
import re

from fabric.api import *


def get_build_config_file():
    """
    Utility function to retrieve the file path to the build configuration file.

    :return:
    """
    # find the build config file path
    current_path = os.path.dirname(os.path.abspath(__file__))
    config_file = current_path + "/config.json"

    return config_file


def get_deployment_config(deployment, config_file=None):
    """
    Utility function to retrieve the build configuration for a certain deployment.

    :param deployment:
    :param config_file:
    :return:
    """
    if config_file is None:
        config_file = get_build_config_file()

    config = json.load(open(config_file))
    deployment_config = config[deployment]
    return deployment_config


def setup(deployment):
    """
    Fabric Task to setup the Fabric env before running deployment tasks from the command line interface.  Example
    usage:

    fab -f build/fabfile.py setup:dev

    will setup the Fabric env to point to the "dev" deployment "backend" server as defined in the configuration file
    "_data/config/build.json".
    :param deployment:
    :return:
    """

    deployment_config = get_deployment_config(deployment)
    target_host = deployment_config["host"]
    env.hosts = [target_host]


def deploy(branch, deployment, deployment_config=None):
    """
    Fabric Task to deploy to some deployment.  Example usage:

    fab -f build/fabfile.py setup:dev deploy:master,dev

    will run the Setup Task to point our env to the appropriate location and then will deploy the master branch to 
    same deployment location and then restart/start up the actual application.

    :param branch:
    :param deployment:
    :param deployment_config:
    :return:
    """
    # load the deployment configuration if it was not specified
    if deployment_config is None:
        deployment_config = get_deployment_config(deployment)

    target_config = deployment_config

    code_dir = '/home/bc/'
    with cd(code_dir):
        if target_config["git_update"]:
            run("git reset HEAD --hard")

            run("git fetch origin")

            run("git checkout " + branch)

            run("git pull origin " + branch)

        if target_config["npm_install"]:
            sudo("npm install")

        if target_config["bower_install"]:
            sudo("bower install --allow-root")

        # move the config.js file for the application
        current_path = os.path.dirname(os.path.abspath(__file__))
        put(current_path + "/../config.js", "config.js");

        # restart the application on port 80 using foreverjs
        run("export PORT=80; forever start app.js")


def halt(deployment, deployment_config=None):
    """
    Fabric Task to halt some deployment.  Example usage:

    fab -f build/fabfile.py setup:dev halt:dev

    will run the Setup Task to point our env to the appropriate location and then will stop any node applications
    we have running under forever.  There should only be ONE thing running under forever which is our application.

    :param deployment:
    :param deployment_config:
    :return:
    """
    # load the deployment configuration if it was not specified
    if deployment_config is None:
        deployment_config = get_deployment_config(deployment)

    target_config = deployment_config

    code_dir = '/home/bc/'
    with cd(code_dir):
        run("forever stopall")
